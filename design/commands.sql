-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `mydb` DEFAULT CHARACTER SET utf8 ;
USE `mydb` ;

-- -----------------------------------------------------
-- Table `mydb`.`User`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`User` (
  `userId` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NULL,
  `gender` INT NULL,
  `dateOfBirth` INT NULL,
  `email` VARCHAR(45) NULL,
  `address` VARCHAR(45) NULL,
  `phoneNumber` INT NULL,
  `city` VARCHAR(45) NULL,
  `state` VARCHAR(45) NULL,
  `pinCode` INT NULL,
  `userPic` LONGBLOB NULL,
  PRIMARY KEY (`userId`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`Request`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`Request` (
  `requestId` INT NOT NULL AUTO_INCREMENT,
  `userId` INT NOT NULL,
  `sourceCountry` VARCHAR(45) NULL,
  `sourceState` VARCHAR(45) NULL,
  `sourceCity` VARCHAR(45) NULL,
  `pickUpLocation` VARCHAR(45) NULL,
  `dateOfPickup` INT NULL,
  `dateOfDelivery` INT NULL,
  `destinationCountry` VARCHAR(45) NULL,
  `destinationState` VARCHAR(45) NULL,
  `destinationCity` VARCHAR(45) NULL,
  `destinationPinCode` INT NULL,
  `sourcePinCode` INT NULL,
  `parcelCategory` INT NULL,
  `parcelWeight` DOUBLE NULL,
  PRIMARY KEY (`requestId`),
  INDEX `fk_request_user_idx` (`userId` ASC),
  CONSTRAINT `fk_request_user`
    FOREIGN KEY (`userId`)
    REFERENCES `mydb`.`User` (`userId`)
    ON DELETE NO ACTION
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`Offer`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`Offer` (
  `offerId` INT NOT NULL AUTO_INCREMENT,
  `userId` INT NOT NULL,
  `sourceCountry` VARCHAR(45) NULL,
  `sourceState` VARCHAR(45) NULL,
  `sourceCity` VARCHAR(45) NULL,
  `pickUpLocation` VARCHAR(45) NULL,
  `dateOfPickup` INT NULL,
  `dateOfDelivery` INT NULL,
  `destinationCountry` VARCHAR(45) NULL,
  `destinationState` VARCHAR(45) NULL,
  `destinationCity` VARCHAR(45) NULL,
  `sourcePinCode` INT NULL,
  `destinationPinCode` INT NULL,
  `mode` INT NULL,
  `pnr` VARCHAR(45) NULL,
  `airline` VARCHAR(45) NULL,
  PRIMARY KEY (`offerId`),
  INDEX `fk_offer_user_idx` (`userId` ASC),
  CONSTRAINT `fk_offer_user`
    FOREIGN KEY (`userId`)
    REFERENCES `mydb`.`User` (`userId`)
    ON DELETE NO ACTION
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`Login`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`Login` (
  `appId` VARCHAR(45) NOT NULL,
  `userId` INT NOT NULL,
  `accessToken` VARCHAR(45) NULL,
  `authToken` VARCHAR(45) NULL,
  PRIMARY KEY (`appId`),
  INDEX `fk_login_user_idx` (`userId` ASC),
  CONSTRAINT `fk_login_user`
    FOREIGN KEY (`userId`)
    REFERENCES `mydb`.`User` (`userId`)
    ON DELETE NO ACTION
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`Booking`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`Booking` (
  `requestId` INT NOT NULL,
  `offerId` INT NOT NULL,
  `sourceCity` VARCHAR(45) NULL,
  `destinationCity` VARCHAR(45) NULL,
  `status` TINYINT(1) NULL,
  `bookingId` INT NOT NULL AUTO_INCREMENT,
  `parcelPic` LONGBLOB NULL,
  PRIMARY KEY (`bookingId`),
  INDEX `requestId_idx` (`requestId` ASC),
  INDEX `fk_booking_offer_idx` (`offerId` ASC),
  CONSTRAINT `fk_booking_request`
    FOREIGN KEY (`requestId`)
    REFERENCES `mydb`.`Request` (`requestId`)
    ON DELETE NO ACTION
    ON UPDATE CASCADE,
  CONSTRAINT `fk_booking_offer`
    FOREIGN KEY (`offerId`)
    REFERENCES `mydb`.`Offer` (`offerId`)
    ON DELETE NO ACTION
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`Tracking`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`Tracking` (
  `trackingId` INT NOT NULL AUTO_INCREMENT,
  `bookingId` INT NOT NULL,
  `latitude` DOUBLE NULL,
  `longitude` DOUBLE NULL,
  PRIMARY KEY (`trackingId`),
  INDEX `fk_tracking_booking_idx` (`bookingId` ASC),
  CONSTRAINT `fk_tracking_booking`
    FOREIGN KEY (`bookingId`)
    REFERENCES `mydb`.`Booking` (`bookingId`)
    ON DELETE NO ACTION
    ON UPDATE CASCADE)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
