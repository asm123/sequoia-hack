//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.5-2 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2016.09.11 at 03:43:46 PM IST 
//


package models;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the models package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: models
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link CreateSearchResponse }
     * 
     */
    public CreateSearchResponse createCreateSearchResponse() {
        return new CreateSearchResponse();
    }

    /**
     * Create an instance of {@link ViewOfferResponse }
     * 
     */
    public ViewOfferResponse createViewOfferResponse() {
        return new ViewOfferResponse();
    }

    /**
     * Create an instance of {@link ViewSearchResponse }
     * 
     */
    public ViewSearchResponse createViewSearchResponse() {
        return new ViewSearchResponse();
    }

    /**
     * Create an instance of {@link CreateOfferResponse }
     * 
     */
    public CreateOfferResponse createCreateOfferResponse() {
        return new CreateOfferResponse();
    }

    /**
     * Create an instance of {@link CarrierRequestInfo }
     * 
     */
    public CarrierRequestInfo createCarrierRequestInfo() {
        return new CarrierRequestInfo();
    }

    /**
     * Create an instance of {@link SenderRequestInfo }
     * 
     */
    public SenderRequestInfo createSenderRequestInfo() {
        return new SenderRequestInfo();
    }

    /**
     * Create an instance of {@link CreateSearchResponse.CarrierOffers }
     * 
     */
    public CreateSearchResponse.CarrierOffers createCreateSearchResponseCarrierOffers() {
        return new CreateSearchResponse.CarrierOffers();
    }

    /**
     * Create an instance of {@link CreateRequestToCarrier }
     * 
     */
    public CreateRequestToCarrier createCreateRequestToCarrier() {
        return new CreateRequestToCarrier();
    }

    /**
     * Create an instance of {@link CreateRequestToSender }
     * 
     */
    public CreateRequestToSender createCreateRequestToSender() {
        return new CreateRequestToSender();
    }

    /**
     * Create an instance of {@link Location }
     * 
     */
    public Location createLocation() {
        return new Location();
    }

    /**
     * Create an instance of {@link ViewOfferResponse.PendingRequestsFromSender }
     * 
     */
    public ViewOfferResponse.PendingRequestsFromSender createViewOfferResponsePendingRequestsFromSender() {
        return new ViewOfferResponse.PendingRequestsFromSender();
    }

    /**
     * Create an instance of {@link ViewOfferResponse.PendingRequestsToSender }
     * 
     */
    public ViewOfferResponse.PendingRequestsToSender createViewOfferResponsePendingRequestsToSender() {
        return new ViewOfferResponse.PendingRequestsToSender();
    }

    /**
     * Create an instance of {@link BookingInfo }
     * 
     */
    public BookingInfo createBookingInfo() {
        return new BookingInfo();
    }

    /**
     * Create an instance of {@link CreateSearchRequest }
     * 
     */
    public CreateSearchRequest createCreateSearchRequest() {
        return new CreateSearchRequest();
    }

    /**
     * Create an instance of {@link ViewSearchResponse.PendingRequestsFromCarrier }
     * 
     */
    public ViewSearchResponse.PendingRequestsFromCarrier createViewSearchResponsePendingRequestsFromCarrier() {
        return new ViewSearchResponse.PendingRequestsFromCarrier();
    }

    /**
     * Create an instance of {@link ViewSearchResponse.PendingRequestsToCarrier }
     * 
     */
    public ViewSearchResponse.PendingRequestsToCarrier createViewSearchResponsePendingRequestsToCarrier() {
        return new ViewSearchResponse.PendingRequestsToCarrier();
    }

    /**
     * Create an instance of {@link ConfirmRequestFromSender }
     * 
     */
    public ConfirmRequestFromSender createConfirmRequestFromSender() {
        return new ConfirmRequestFromSender();
    }

    /**
     * Create an instance of {@link CreateOfferRequest }
     * 
     */
    public CreateOfferRequest createCreateOfferRequest() {
        return new CreateOfferRequest();
    }

    /**
     * Create an instance of {@link ConfirmRequestFromCarrier }
     * 
     */
    public ConfirmRequestFromCarrier createConfirmRequestFromCarrier() {
        return new ConfirmRequestFromCarrier();
    }

    /**
     * Create an instance of {@link CreateOfferResponse.SenderRequests }
     * 
     */
    public CreateOfferResponse.SenderRequests createCreateOfferResponseSenderRequests() {
        return new CreateOfferResponse.SenderRequests();
    }

    /**
     * Create an instance of {@link Person }
     * 
     */
    public Person createPerson() {
        return new Person();
    }

    /**
     * Create an instance of {@link CarrierRequestInfo.RequestInfo }
     * 
     */
    public CarrierRequestInfo.RequestInfo createCarrierRequestInfoRequestInfo() {
        return new CarrierRequestInfo.RequestInfo();
    }

    /**
     * Create an instance of {@link SenderRequestInfo.RequestInfo }
     * 
     */
    public SenderRequestInfo.RequestInfo createSenderRequestInfoRequestInfo() {
        return new SenderRequestInfo.RequestInfo();
    }

}
