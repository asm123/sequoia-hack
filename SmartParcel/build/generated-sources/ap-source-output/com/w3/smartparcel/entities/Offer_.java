package com.w3.smartparcel.entities;

import com.w3.smartparcel.entities.Booking;
import com.w3.smartparcel.entities.Option;
import com.w3.smartparcel.entities.UserInfo;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2016-09-11T15:43:47")
@StaticMetamodel(Offer.class)
public class Offer_ { 

    public static volatile SingularAttribute<Offer, Double> sourceLatitude;
    public static volatile SingularAttribute<Offer, UserInfo> userInfo;
    public static volatile SingularAttribute<Offer, Integer> dateOfPickup;
    public static volatile SingularAttribute<Offer, Integer> dateOfDelivery;
    public static volatile ListAttribute<Offer, Option> optionList;
    public static volatile SingularAttribute<Offer, Double> weight;
    public static volatile SingularAttribute<Offer, Double> destinationLongitude;
    public static volatile SingularAttribute<Offer, Integer> mode;
    public static volatile SingularAttribute<Offer, Double> sourceLongitude;
    public static volatile ListAttribute<Offer, Booking> bookingList;
    public static volatile SingularAttribute<Offer, String> destinationName;
    public static volatile SingularAttribute<Offer, Integer> offerId;
    public static volatile SingularAttribute<Offer, String> sourceName;
    public static volatile SingularAttribute<Offer, Double> destinationLatitude;
    public static volatile SingularAttribute<Offer, Integer> status;

}