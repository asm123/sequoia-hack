package com.w3.smartparcel.entities;

import com.w3.smartparcel.entities.Booking;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2016-09-11T15:43:47")
@StaticMetamodel(Tracking.class)
public class Tracking_ { 

    public static volatile SingularAttribute<Tracking, Double> latitude;
    public static volatile SingularAttribute<Tracking, Integer> time;
    public static volatile SingularAttribute<Tracking, Booking> bookingId;
    public static volatile SingularAttribute<Tracking, Integer> trackingId;
    public static volatile SingularAttribute<Tracking, Double> longitude;

}