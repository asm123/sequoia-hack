package com.w3.smartparcel.entities;

import com.w3.smartparcel.entities.Booking;
import com.w3.smartparcel.entities.Login;
import com.w3.smartparcel.entities.Offer;
import com.w3.smartparcel.entities.Search;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2016-09-11T15:43:47")
@StaticMetamodel(UserInfo.class)
public class UserInfo_ { 

    public static volatile SingularAttribute<UserInfo, String> address;
    public static volatile SingularAttribute<UserInfo, Integer> phoneNumber;
    public static volatile SingularAttribute<UserInfo, Integer> gender;
    public static volatile SingularAttribute<UserInfo, String> city;
    public static volatile ListAttribute<UserInfo, Search> searchList;
    public static volatile ListAttribute<UserInfo, Login> loginList;
    public static volatile ListAttribute<UserInfo, Booking> bookingList;
    public static volatile SingularAttribute<UserInfo, String> name;
    public static volatile ListAttribute<UserInfo, Offer> offerList;
    public static volatile SingularAttribute<UserInfo, Integer> userId;
    public static volatile SingularAttribute<UserInfo, byte[]> picture;
    public static volatile SingularAttribute<UserInfo, String> email;

}