package com.w3.smartparcel.entities;

import com.w3.smartparcel.entities.Offer;
import com.w3.smartparcel.entities.Search;
import com.w3.smartparcel.entities.Tracking;
import com.w3.smartparcel.entities.UserInfo;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2016-09-11T15:43:47")
@StaticMetamodel(Booking.class)
public class Booking_ { 

    public static volatile SingularAttribute<Booking, Offer> offer;
    public static volatile SingularAttribute<Booking, UserInfo> userInfo;
    public static volatile SingularAttribute<Booking, Search> search;
    public static volatile ListAttribute<Booking, Tracking> trackingList;
    public static volatile SingularAttribute<Booking, Integer> bookingId;
    public static volatile SingularAttribute<Booking, Integer> status;

}