package com.w3.smartparcel.entities;

import com.w3.smartparcel.entities.UserInfo;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2016-09-11T15:43:47")
@StaticMetamodel(Login.class)
public class Login_ { 

    public static volatile SingularAttribute<Login, String> appId;
    public static volatile SingularAttribute<Login, String> authToken;
    public static volatile SingularAttribute<Login, String> accessToken;
    public static volatile SingularAttribute<Login, UserInfo> userId;

}