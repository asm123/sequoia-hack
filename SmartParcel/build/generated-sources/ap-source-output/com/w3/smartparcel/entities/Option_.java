package com.w3.smartparcel.entities;

import com.w3.smartparcel.entities.Offer;
import com.w3.smartparcel.entities.Search;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2016-09-11T15:43:47")
@StaticMetamodel(Option.class)
public class Option_ { 

    public static volatile SingularAttribute<Option, Integer> optionType;
    public static volatile SingularAttribute<Option, Offer> offer;
    public static volatile SingularAttribute<Option, Search> search;
    public static volatile SingularAttribute<Option, Integer> optionId;
    public static volatile SingularAttribute<Option, Integer> status;

}