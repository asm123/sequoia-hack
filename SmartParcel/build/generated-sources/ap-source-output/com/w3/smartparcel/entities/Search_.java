package com.w3.smartparcel.entities;

import com.w3.smartparcel.entities.Booking;
import com.w3.smartparcel.entities.Option;
import com.w3.smartparcel.entities.UserInfo;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2016-09-11T15:43:47")
@StaticMetamodel(Search.class)
public class Search_ { 

    public static volatile SingularAttribute<Search, String> parcelDescription;
    public static volatile SingularAttribute<Search, Double> sourceLatitude;
    public static volatile SingularAttribute<Search, UserInfo> userInfo;
    public static volatile SingularAttribute<Search, Integer> dateOfPickup;
    public static volatile SingularAttribute<Search, Integer> dateOfDelivery;
    public static volatile ListAttribute<Search, Option> optionList;
    public static volatile SingularAttribute<Search, Double> destinationLongitude;
    public static volatile SingularAttribute<Search, Double> sourceLongitude;
    public static volatile SingularAttribute<Search, Integer> searchId;
    public static volatile ListAttribute<Search, Booking> bookingList;
    public static volatile SingularAttribute<Search, String> destinationName;
    public static volatile SingularAttribute<Search, Double> parcelWeight;
    public static volatile SingularAttribute<Search, String> sourceName;
    public static volatile SingularAttribute<Search, Double> destinationLatitude;
    public static volatile SingularAttribute<Search, byte[]> parcelPicture;

}