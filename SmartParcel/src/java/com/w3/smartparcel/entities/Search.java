/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.w3.smartparcel.entities;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author asmita
 */
@Entity
@Table(name = "search")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Search.findAll", query = "SELECT s FROM Search s"),
    @NamedQuery(name = "Search.findBySearchId", query = "SELECT s FROM Search s WHERE s.searchId = :searchId"),
    @NamedQuery(name = "Search.findByDateOfPickup", query = "SELECT s FROM Search s WHERE s.dateOfPickup = :dateOfPickup"),
    @NamedQuery(name = "Search.findByDateOfDelivery", query = "SELECT s FROM Search s WHERE s.dateOfDelivery = :dateOfDelivery"),
    @NamedQuery(name = "Search.findByParcelWeight", query = "SELECT s FROM Search s WHERE s.parcelWeight = :parcelWeight"),
    @NamedQuery(name = "Search.findByParcelDescription", query = "SELECT s FROM Search s WHERE s.parcelDescription = :parcelDescription"),
    @NamedQuery(name = "Search.findBySourceLatitude", query = "SELECT s FROM Search s WHERE s.sourceLatitude = :sourceLatitude"),
    @NamedQuery(name = "Search.findBySourceLongitude", query = "SELECT s FROM Search s WHERE s.sourceLongitude = :sourceLongitude"),
    @NamedQuery(name = "Search.findBySourceName", query = "SELECT s FROM Search s WHERE s.sourceName = :sourceName"),
    @NamedQuery(name = "Search.findByDestinationLatitude", query = "SELECT s FROM Search s WHERE s.destinationLatitude = :destinationLatitude"),
    @NamedQuery(name = "Search.findByDestinationLongitude", query = "SELECT s FROM Search s WHERE s.destinationLongitude = :destinationLongitude"),
    @NamedQuery(name = "Search.findByDestinationName", query = "SELECT s FROM Search s WHERE s.destinationName = :destinationName")})
public class Search implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "search_id")
    private Integer searchId;
    @Column(name = "date_of_pickup")
    private Integer dateOfPickup;
    @Column(name = "date_of_delivery")
    private Integer dateOfDelivery;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "parcel_weight")
    private Double parcelWeight;
    @Size(max = 2147483647)
    @Column(name = "parcel_description")
    private String parcelDescription;
    @Lob
    @Column(name = "parcel_picture")
    private byte[] parcelPicture;
    @Column(name = "source_latitude")
    private Double sourceLatitude;
    @Column(name = "source_longitude")
    private Double sourceLongitude;
    @Size(max = 2147483647)
    @Column(name = "source_name")
    private String sourceName;
    @Column(name = "destination_latitude")
    private Double destinationLatitude;
    @Column(name = "destination_longitude")
    private Double destinationLongitude;
    @Size(max = 2147483647)
    @Column(name = "destination_name")
    private String destinationName;
    @OneToMany(mappedBy = "search")
    private List<Booking> bookingList;
    @JoinColumn(name = "user_info", referencedColumnName = "user_id")
    @ManyToOne
    private UserInfo userInfo;
    @OneToMany(mappedBy = "search")
    private List<Option> optionList;

    public Search() {
    }

    public Search(Integer searchId) {
        this.searchId = searchId;
    }

    public Integer getSearchId() {
        return searchId;
    }

    public void setSearchId(Integer searchId) {
        this.searchId = searchId;
    }

    public Integer getDateOfPickup() {
        return dateOfPickup;
    }

    public void setDateOfPickup(Integer dateOfPickup) {
        this.dateOfPickup = dateOfPickup;
    }

    public Integer getDateOfDelivery() {
        return dateOfDelivery;
    }

    public void setDateOfDelivery(Integer dateOfDelivery) {
        this.dateOfDelivery = dateOfDelivery;
    }

    public Double getParcelWeight() {
        return parcelWeight;
    }

    public void setParcelWeight(Double parcelWeight) {
        this.parcelWeight = parcelWeight;
    }

    public String getParcelDescription() {
        return parcelDescription;
    }

    public void setParcelDescription(String parcelDescription) {
        this.parcelDescription = parcelDescription;
    }

    public byte[] getParcelPicture() {
        return parcelPicture;
    }

    public void setParcelPicture(byte[] parcelPicture) {
        this.parcelPicture = parcelPicture;
    }

    public Double getSourceLatitude() {
        return sourceLatitude;
    }

    public void setSourceLatitude(Double sourceLatitude) {
        this.sourceLatitude = sourceLatitude;
    }

    public Double getSourceLongitude() {
        return sourceLongitude;
    }

    public void setSourceLongitude(Double sourceLongitude) {
        this.sourceLongitude = sourceLongitude;
    }

    public String getSourceName() {
        return sourceName;
    }

    public void setSourceName(String sourceName) {
        this.sourceName = sourceName;
    }

    public Double getDestinationLatitude() {
        return destinationLatitude;
    }

    public void setDestinationLatitude(Double destinationLatitude) {
        this.destinationLatitude = destinationLatitude;
    }

    public Double getDestinationLongitude() {
        return destinationLongitude;
    }

    public void setDestinationLongitude(Double destinationLongitude) {
        this.destinationLongitude = destinationLongitude;
    }

    public String getDestinationName() {
        return destinationName;
    }

    public void setDestinationName(String destinationName) {
        this.destinationName = destinationName;
    }

    @XmlTransient
    public List<Booking> getBookingList() {
        return bookingList;
    }

    public void setBookingList(List<Booking> bookingList) {
        this.bookingList = bookingList;
    }

    public UserInfo getUserInfo() {
        return userInfo;
    }

    public void setUserInfo(UserInfo userInfo) {
        this.userInfo = userInfo;
    }

    @XmlTransient
    public List<Option> getOptionList() {
        return optionList;
    }

    public void setOptionList(List<Option> optionList) {
        this.optionList = optionList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (searchId != null ? searchId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Search)) {
            return false;
        }
        Search other = (Search) object;
        if ((this.searchId == null && other.searchId != null) || (this.searchId != null && !this.searchId.equals(other.searchId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.w3.smartparcel.entities.Search[ searchId=" + searchId + " ]";
    }
    
}
