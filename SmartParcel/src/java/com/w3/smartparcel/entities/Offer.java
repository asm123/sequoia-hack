/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.w3.smartparcel.entities;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author asmita
 */
@Entity
@Table(name = "offer")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Offer.findAll", query = "SELECT o FROM Offer o"),
    @NamedQuery(name = "Offer.findByOfferId", query = "SELECT o FROM Offer o WHERE o.offerId = :offerId"),
    @NamedQuery(name = "Offer.findByDateOfPickup", query = "SELECT o FROM Offer o WHERE o.dateOfPickup = :dateOfPickup"),
    @NamedQuery(name = "Offer.findByDateOfDelivery", query = "SELECT o FROM Offer o WHERE o.dateOfDelivery = :dateOfDelivery"),
    @NamedQuery(name = "Offer.findByMode", query = "SELECT o FROM Offer o WHERE o.mode = :mode"),
    @NamedQuery(name = "Offer.findByWeight", query = "SELECT o FROM Offer o WHERE o.weight = :weight"),
    @NamedQuery(name = "Offer.findByStatus", query = "SELECT o FROM Offer o WHERE o.status = :status"),
    @NamedQuery(name = "Offer.findBySourceLatitude", query = "SELECT o FROM Offer o WHERE o.sourceLatitude = :sourceLatitude"),
    @NamedQuery(name = "Offer.findBySourceLongitude", query = "SELECT o FROM Offer o WHERE o.sourceLongitude = :sourceLongitude"),
    @NamedQuery(name = "Offer.findBySourceName", query = "SELECT o FROM Offer o WHERE o.sourceName = :sourceName"),
    @NamedQuery(name = "Offer.findByDestinationLatitude", query = "SELECT o FROM Offer o WHERE o.destinationLatitude = :destinationLatitude"),
    @NamedQuery(name = "Offer.findByDestinationLongitude", query = "SELECT o FROM Offer o WHERE o.destinationLongitude = :destinationLongitude"),
    @NamedQuery(name = "Offer.findByDestinationName", query = "SELECT o FROM Offer o WHERE o.destinationName = :destinationName")})
public class Offer implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "offer_id")
    private Integer offerId;
    @Column(name = "date_of_pickup")
    private Integer dateOfPickup;
    @Column(name = "date_of_delivery")
    private Integer dateOfDelivery;
    @Column(name = "mode")
    private Integer mode;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "weight")
    private Double weight;
    @Column(name = "status")
    private Integer status;
    @Column(name = "source_latitude")
    private Double sourceLatitude;
    @Column(name = "source_longitude")
    private Double sourceLongitude;
    @Size(max = 2147483647)
    @Column(name = "source_name")
    private String sourceName;
    @Column(name = "destination_latitude")
    private Double destinationLatitude;
    @Column(name = "destination_longitude")
    private Double destinationLongitude;
    @Size(max = 2147483647)
    @Column(name = "destination_name")
    private String destinationName;
    @JoinColumn(name = "user_info", referencedColumnName = "user_id")
    @ManyToOne
    private UserInfo userInfo;
    @OneToMany(mappedBy = "offer")
    private List<Booking> bookingList;
    @OneToMany(mappedBy = "offer")
    private List<Option> optionList;

    public Offer() {
    }

    public Offer(Integer offerId) {
        this.offerId = offerId;
    }

    public Integer getOfferId() {
        return offerId;
    }

    public void setOfferId(Integer offerId) {
        this.offerId = offerId;
    }

    public Integer getDateOfPickup() {
        return dateOfPickup;
    }

    public void setDateOfPickup(Integer dateOfPickup) {
        this.dateOfPickup = dateOfPickup;
    }

    public Integer getDateOfDelivery() {
        return dateOfDelivery;
    }

    public void setDateOfDelivery(Integer dateOfDelivery) {
        this.dateOfDelivery = dateOfDelivery;
    }

    public Integer getMode() {
        return mode;
    }

    public void setMode(Integer mode) {
        this.mode = mode;
    }

    public Double getWeight() {
        return weight;
    }

    public void setWeight(Double weight) {
        this.weight = weight;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Double getSourceLatitude() {
        return sourceLatitude;
    }

    public void setSourceLatitude(Double sourceLatitude) {
        this.sourceLatitude = sourceLatitude;
    }

    public Double getSourceLongitude() {
        return sourceLongitude;
    }

    public void setSourceLongitude(Double sourceLongitude) {
        this.sourceLongitude = sourceLongitude;
    }

    public String getSourceName() {
        return sourceName;
    }

    public void setSourceName(String sourceName) {
        this.sourceName = sourceName;
    }

    public Double getDestinationLatitude() {
        return destinationLatitude;
    }

    public void setDestinationLatitude(Double destinationLatitude) {
        this.destinationLatitude = destinationLatitude;
    }

    public Double getDestinationLongitude() {
        return destinationLongitude;
    }

    public void setDestinationLongitude(Double destinationLongitude) {
        this.destinationLongitude = destinationLongitude;
    }

    public String getDestinationName() {
        return destinationName;
    }

    public void setDestinationName(String destinationName) {
        this.destinationName = destinationName;
    }

    public UserInfo getUserInfo() {
        return userInfo;
    }

    public void setUserInfo(UserInfo userInfo) {
        this.userInfo = userInfo;
    }

    @XmlTransient
    public List<Booking> getBookingList() {
        return bookingList;
    }

    public void setBookingList(List<Booking> bookingList) {
        this.bookingList = bookingList;
    }

    @XmlTransient
    public List<Option> getOptionList() {
        return optionList;
    }

    public void setOptionList(List<Option> optionList) {
        this.optionList = optionList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (offerId != null ? offerId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Offer)) {
            return false;
        }
        Offer other = (Offer) object;
        if ((this.offerId == null && other.offerId != null) || (this.offerId != null && !this.offerId.equals(other.offerId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.w3.smartparcel.entities.Offer[ offerId=" + offerId + " ]";
    }
    
}
