/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.w3.smartparcel.utils;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

/**
 *
 * @author asmita
 */
@Entity
@NamedQueries({
    @NamedQuery(name = "Offer.findByPickupInDuration", query = "SELECT o FROM Offer o WHERE o.dateOfPickup between :startTime and :endTime"),
    @NamedQuery(name = "Offer.findByDeliveryInDuration", query = "SELECT o FROM Offer o WHERE o.dateOfDelivery between :startTime and :endTime"),
    @NamedQuery(name = "Search.findByPickupInDuration", query = "SELECT s FROM Search s WHERE s.dateOfPickup between :startTime and :endTime"),
    @NamedQuery(name = "Search.findByDeliveryInDuration", query = "SELECT s FROM Search s WHERE s.dateOfDelivery between :startTime and :endTime")
})
public class NamedQueriesHolder implements Serializable {

    @Id
    private Integer id;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
    
}
