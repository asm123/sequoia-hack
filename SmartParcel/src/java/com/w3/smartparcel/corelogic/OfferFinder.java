/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.w3.smartparcel.corelogic;

import com.w3.smartparcel.entities.Offer;
import com.w3.smartparcel.entities.Option;
import com.w3.smartparcel.entities.Search;
import com.w3.smartparcel.enums.OptionStatus;
import com.w3.smartparcel.enums.OptionType;
import com.w3.smartparcel.utils.Constants;
import com.w3.smartparcel.utils.DistanceUtils;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.persistence.EntityManager;

/**
 *
 * @author asmita
 */
public class OfferFinder {
    
    private EntityManager em;
    
    public OfferFinder(EntityManager em) {
        this.em = em;
    }
    
    public List<Option> findOptions(Search search) {
        List<Option> options = new ArrayList<>();
        Option option;
        List<Offer> offers;
        int requestedPickupTime, startPickupDuration, endPickupDuration;
        int requestedDeliveryTime, startDeliveryDuration, endDeliveryDuration;
        
        requestedPickupTime = search.getDateOfPickup();
        // 1 hrs before the specified time
        startPickupDuration = requestedPickupTime - 1 * 60 * 60;
        // 2 hrs after the specified time
        endPickupDuration = requestedPickupTime + 2 * 60 * 60;
        
        requestedDeliveryTime = search.getDateOfDelivery();
        startDeliveryDuration = requestedDeliveryTime - 1 * 60 * 60;
        endDeliveryDuration = requestedDeliveryTime + 2 * 60 * 60;
        
        // find offers which fulfil the pickup time criterion
        offers = em.createNamedQuery("Offer.findByPickupInDuration", Offer.class)
                .setParameter("startTime", startPickupDuration)
                .setParameter("endTime", endPickupDuration)
                .getResultList();
        
        if (offers.isEmpty()) {
            return options;
        }
        
        // for each of these offers, pick only those which fulfil the delivery time criterion
        for (Offer offer: offers) {
            if (offer.getDateOfDelivery() >= startDeliveryDuration && offer.getDateOfDelivery() <= endDeliveryDuration
                    && offer.getDateOfDelivery() >= requestedPickupTime) {
                
                double sourceDistance = DistanceUtils.distance(search.getSourceLatitude(), search.getSourceLongitude(), 
                        offer.getSourceLatitude(), offer.getSourceLongitude());
                
                double destinationDistance = DistanceUtils.distance(search.getDestinationLatitude(), search.getDestinationLongitude(), 
                        offer.getDestinationLatitude(), offer.getDestinationLongitude());
                
                if (sourceDistance > Constants.DISTANCE_THRESHOLD || destinationDistance > Constants.DISTANCE_THRESHOLD) {
                    continue;
                }
                
                option = new Option();
                option.setOffer(offer);
                option.setOptionType(OptionType.REQUEST_TO_CARRIER.getValue());
                option.setSearch(search);
                option.setStatus(OptionStatus.CREATED.getValue());
                options.add(option);
                
                em.persist(option);
                em.flush();
                
                offer.getOptionList().add(option);
                em.persist(offer);
            }
        }
        
        em.flush();
        
        return options;
    }
}
