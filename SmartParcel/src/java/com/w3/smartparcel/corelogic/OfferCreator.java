/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.w3.smartparcel.corelogic;

import com.w3.smartparcel.entities.Offer;
import com.w3.smartparcel.entities.Option;
import com.w3.smartparcel.entities.Search;
import com.w3.smartparcel.enums.OptionStatus;
import com.w3.smartparcel.enums.OptionType;
import com.w3.smartparcel.utils.Constants;
import com.w3.smartparcel.utils.DistanceUtils;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;

/**
 *
 * @author asmita
 */
public class OfferCreator {
    
    private EntityManager em;
    
    public OfferCreator(EntityManager em) {
        this.em = em;
    }
    
    public List<Option> findOptions(Offer offer) {
        List<Option> options = new ArrayList<>();
        Option option;
        List<Search> searches;
        
        int offeredPickupTime, startPickupDuration, endPickupDuration;
        int offeredDeliveryTime, startDeliveryDuration, endDeliveryDuration;
        
        offeredPickupTime = offer.getDateOfPickup();
        // 12 hrs before the specified time
        startPickupDuration = offeredPickupTime - 2 * 60 * 60;
        // 2 hrs after the specified time
        endPickupDuration = offeredPickupTime + 1 * 60 * 60;
        
        offeredDeliveryTime = offer.getDateOfDelivery();
        startDeliveryDuration = offeredDeliveryTime - 1 * 60 * 60;
        endDeliveryDuration = offeredDeliveryTime + 2 * 60 * 60;
        
        // find searches which fulfil the pickup time duration
        searches = em.createNamedQuery("Search.findByPickupInDuration", Search.class)
                .setParameter("startTime", startPickupDuration)
                .setParameter("endTime", endPickupDuration)
                .getResultList();
        
        if (searches.isEmpty()) {
            return options;
        }
        
        // for each of these searches, pick only those which fulfil the delivery time criterion
        for (Search search: searches) {
            if (search.getDateOfDelivery() >= startDeliveryDuration && search.getDateOfDelivery() <= endDeliveryDuration
                    && search.getDateOfDelivery() > offeredPickupTime) {
                
                double sourceDistance = DistanceUtils.distance(search.getSourceLatitude(), search.getSourceLongitude(), 
                        offer.getSourceLatitude(), offer.getSourceLongitude());
                
                double destinationDistance = DistanceUtils.distance(search.getDestinationLatitude(), search.getDestinationLongitude(), 
                        offer.getDestinationLatitude(), offer.getDestinationLongitude());
                
                if (sourceDistance > Constants.DISTANCE_THRESHOLD || destinationDistance > Constants.DISTANCE_THRESHOLD) {
                    continue;
                }
                
                option = new Option();
                option.setSearch(search);
                option.setOptionType(OptionType.REQUEST_TO_SENDER.getValue());
                option.setOffer(offer);
                option.setStatus(OptionStatus.CREATED.getValue());
                options.add(option);
                
                em.persist(option);
                em.flush();
                
                search.getOptionList().add(option);
                em.persist(search);
            }
        }
        
        em.flush();
        
        return options;
    }
}
