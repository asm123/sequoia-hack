/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.w3.smartparcel.enums;

/**
 *
 * @author asmita
 */
public enum OptionType {
    REQUEST_TO_SENDER(0),
    REQUEST_TO_CARRIER(1)
    ;
    
    private int value;
    
    private OptionType(int value) {
        this.value = value;
    }
    
    public int getValue() {
        return this.value;
    }
}
