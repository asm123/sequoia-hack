/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.w3.smartparcel.enums;

/**
 *
 * @author asmita
 */
public enum Mode {
    TRAIN(0),
    FLIGHT(1),
    CAR(2)
    ;
    
    private int value;
    
    private Mode(int value) {
        this.value = value;
    }
    
    public int getValue() {
        return this.value;
    }
    
    public Mode getMode(String mode) {
        return Mode.valueOf(mode.toUpperCase());
    }
}
