/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.w3.smartparcel.enums;

/**
 *
 * @author asmita
 */
public enum ParcelCategory {
    FOOD(0),
    MEDICINE(1),
    BOOK(2),
    ELECTRONICS(3),
    DOCUMENTS(4);
    
    private int category;
    
    private ParcelCategory(int category) {
        this.category = category;
    }
    
    public int getCategory() {
        return this.category;
    }
    
    public int getCategory(String category) {
        return ParcelCategory.valueOf(category.toUpperCase()).getCategory();
    }
}
