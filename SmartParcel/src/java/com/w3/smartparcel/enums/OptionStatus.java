/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.w3.smartparcel.enums;

/**
 *
 * @author asmita
 */
public enum OptionStatus {
    CREATED(0),
    REQUESTED(1),
    CANCELLED(2),
    INVALIDATED(3),
    REJECTED(4),
    CONFIRMED(5)
    ;
    
    private int value;
    
    private OptionStatus(int value) {
        this.value = value;
    }
    
    public int getValue() {
        return this.value;
    }
}
