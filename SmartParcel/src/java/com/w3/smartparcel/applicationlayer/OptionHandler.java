/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.w3.smartparcel.applicationlayer;

import com.w3.smartparcel.entities.Booking;
import com.w3.smartparcel.entities.Offer;
import com.w3.smartparcel.entities.Option;
import com.w3.smartparcel.entities.Search;
import com.w3.smartparcel.entities.UserInfo;
import com.w3.smartparcel.enums.OptionStatus;
import com.w3.smartparcel.enums.OptionType;
import com.w3.smartparcel.servicemodels.ConfirmRequestFromSender;
import com.w3.smartparcel.servicemodels.CreateRequestToCarrier;
import com.w3.smartparcel.servicemodels.CreateRequestToSender;
import javax.persistence.EntityManager;

/**
 *
 * @author asmita
 */
public class OptionHandler {
    
    private EntityManager em;
    
    public OptionHandler(EntityManager em) {
        this.em = em;
    }
    
    public boolean sendRequestToCarrier(CreateRequestToCarrier request, UserInfo sender) {
        boolean status;
        Search search;
        Option option;
        
        try {
            option = em.find(Option.class, request.getCarrierRequestId());
            search = em.find(Search.class, request.getSearchId());

            if (option.getOptionType() != OptionType.REQUEST_TO_CARRIER.getValue() || !option.getSearch().equals(search)) {
                status = false;
            }
            else {
                option.setStatus(OptionStatus.REQUESTED.getValue());
                em.persist(option);
                em.flush();
                status = true;
            }
        } catch (Exception e) {
            e.printStackTrace();
            status = false;
        }
        return status;
    }
    
    public boolean sendRequestToSender(CreateRequestToSender request, UserInfo carrier) {
        boolean status;
        Offer offer;
        Option option;
        
        try {
            option = em.find(Option.class, request.getSenderRequestId());
            offer = em.find(Offer.class, request.getOfferId());

            if (option.getOptionType() != OptionType.REQUEST_TO_SENDER.getValue() || !option.getOffer().equals(offer)) {
                status = false;
            }
            else {
                option.setStatus(OptionStatus.REQUESTED.getValue());
                em.persist(option);
                em.flush();
                status = true;
            }
        } catch (Exception e) {
            e.printStackTrace();
            status = false;
        }
        return status;
    }
    
    public boolean confirmRequestFromSender(ConfirmRequestFromSender request, UserInfo carrier, int requestId) {
        boolean status, confirm;
        Search search;
        Option option;
        Booking booking;
        
        try {
            option = em.find(Option.class, requestId);
            confirm = request.isStatus();
            if (!confirm) {
                option.setStatus(OptionStatus.REJECTED.getValue());
            }
            else {
                option.setStatus(OptionStatus.CONFIRMED.getValue());
                booking = new BookingHandler(em).createBooking();
                booking.setSearch(option.getSearch());
                booking.setOffer(option.getOffer());
                em.persist(booking);
            }
            em.persist(option);
            em.flush();
            status = true;
        } catch (Exception e) {
            e.printStackTrace();
            status = false;
        }
        return status;
    }
}
