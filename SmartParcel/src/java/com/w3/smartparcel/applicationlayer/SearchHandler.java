/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.w3.smartparcel.applicationlayer;

import com.w3.smartparcel.corelogic.OfferFinder;
import com.w3.smartparcel.entities.Option;
import com.w3.smartparcel.entities.Search;
import com.w3.smartparcel.parsers.OptionParser;
import com.w3.smartparcel.servicemodels.CreateSearchResponse;
import java.util.List;
import javax.persistence.EntityManager;

/**
 *
 * @author asmita
 */
public class SearchHandler {
    
    private EntityManager em;
    
    public SearchHandler(EntityManager em) {
        this.em = em;
    }
    
    public CreateSearchResponse searchOffers(Search search) {
        CreateSearchResponse response;
        OfferFinder offerFinder;
        List<Option> options;
        CreateSearchResponse.CarrierOffers carrierOffers;
        
        em.persist(search);
        em.flush();
        
        response = new CreateSearchResponse();
        
        offerFinder = new OfferFinder(em);
        options = offerFinder.findOptions(search);
        carrierOffers = OptionParser.optionsToCarrierOffers(options);
        
        response.setSearchId(search.getSearchId());
        response.setCarrierOffers(carrierOffers);
        
        return response;
    }
}
