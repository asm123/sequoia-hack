/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.w3.smartparcel.applicationlayer;

import com.w3.smartparcel.corelogic.OfferCreator;
import com.w3.smartparcel.entities.Offer;
import com.w3.smartparcel.entities.Option;
import com.w3.smartparcel.parsers.OptionParser;
import com.w3.smartparcel.servicemodels.CreateOfferResponse;
import java.util.List;
import javax.persistence.EntityManager;

/**
 *
 * @author asmita
 */
public class OfferHandler {
    
    private EntityManager em;
    
    public OfferHandler(EntityManager em) {
        this.em = em;
    }
    
    public CreateOfferResponse createAnOffer(Offer offer) {
        CreateOfferResponse response;
        OfferCreator offerCreator;
        List<Option> options;
        CreateOfferResponse.SenderRequests senderRequests;
        
        em.persist(offer);
        em.flush();
        
        response = new CreateOfferResponse();
        
        offerCreator = new OfferCreator(em);
        options = offerCreator.findOptions(offer);
        senderRequests = OptionParser.optionsToSenderRequests(options);
        
        response.setOfferId(offer.getOfferId());
        response.setSenderRequests(senderRequests);
        
        return response;
    }
}
