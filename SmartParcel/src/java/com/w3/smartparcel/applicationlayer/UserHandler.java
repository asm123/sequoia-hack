/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.w3.smartparcel.applicationlayer;

import com.w3.smartparcel.entities.UserInfo;
import javax.persistence.EntityManager;

/**
 *
 * @author asmita
 */
public class UserHandler {
    
    public static UserInfo getUser(EntityManager em, int userId) {
        UserInfo user = em.find(UserInfo.class, userId);
        return user;
    }
    
}
