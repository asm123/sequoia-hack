/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.w3.smartparcel.parsers;

import com.w3.smartparcel.entities.Offer;
import com.w3.smartparcel.entities.UserInfo;
import com.w3.smartparcel.enums.Mode;
import com.w3.smartparcel.servicemodels.CreateOfferRequest;

/**
 *
 * @author asmita
 */
public class OfferParser {
    
    public static Offer serviceModelToEntity(CreateOfferRequest request, UserInfo carrier) {
        Offer offer = new Offer();
        
        offer.setUserInfo(carrier);
        offer.setSourceLatitude(request.getSource().getLatitude());
        offer.setSourceLongitude(request.getSource().getLongitude());
        offer.setDestinationLatitude(request.getDestination().getLatitude());
        offer.setDestinationLongitude(request.getDestination().getLongitude());
        offer.setDateOfPickup(request.getPickupTime());
        offer.setDateOfDelivery(request.getDeliveryTime());
        offer.setMode(Mode.valueOf(request.getMode()).getValue());
        offer.setWeight(request.getMaximumParcelWeight());
        
        return offer;
    }
}
