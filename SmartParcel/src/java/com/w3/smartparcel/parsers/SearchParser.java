/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.w3.smartparcel.parsers;

import com.w3.smartparcel.entities.Search;
import com.w3.smartparcel.entities.UserInfo;
import com.w3.smartparcel.enums.ParcelCategory;
import com.w3.smartparcel.servicemodels.CreateSearchRequest;
import com.w3.smartparcel.servicemodels.CreateSearchResponse;

/**
 *
 * @author asmita
 */
public class SearchParser {
    
    
    public static Search serviceModelToEntity(CreateSearchRequest searchRequest, UserInfo sender) {
        Search search = new Search();
        
        search.setUserInfo(sender);
        search.setSourceLatitude(searchRequest.getSource().getLatitude());
        search.setSourceLongitude(searchRequest.getSource().getLongitude());
        search.setDestinationLatitude(searchRequest.getDestination().getLatitude());
        search.setDestinationLongitude(searchRequest.getDestination().getLongitude());
        search.setDateOfPickup(searchRequest.getPickupTime());
//        search.setParcelCategory(ParcelCategory.valueOf(searchRequest.getParcelCategory().toUpperCase()).getCategory());
        search.setDateOfDelivery(searchRequest.getDeliveryTime());
        search.setParcelWeight(searchRequest.getParcelWeight());
        search.setParcelDescription(searchRequest.getParcelDescription());
        /*
        if (searchRequest.getParcelPicture() != null) {
            search.setParcelPicture(searchRequest.getParcelPicture().getBytes());
        }
        */
        return search;
    }
    
    public static CreateSearchResponse entityToServiceModel(Search search) {
        CreateSearchResponse searchResponse = new CreateSearchResponse();
        
        
        
        return searchResponse;
    }
}
