/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.w3.smartparcel.parsers;

import com.w3.smartparcel.entities.Option;
import com.w3.smartparcel.enums.ParcelCategory;
import com.w3.smartparcel.servicemodels.CarrierRequestInfo;
import com.w3.smartparcel.servicemodels.CreateOfferResponse;
import com.w3.smartparcel.servicemodels.CreateSearchResponse;
import com.w3.smartparcel.servicemodels.Location;
import com.w3.smartparcel.servicemodels.Person;
import com.w3.smartparcel.servicemodels.SenderRequestInfo;
import java.util.List;

/**
 *
 * @author asmita
 */
public class OptionParser {
    
    public static CarrierRequestInfo optionToCarrierRequestInfo(Option option) {
        CarrierRequestInfo carrierRequestInfo;
        CarrierRequestInfo.RequestInfo requestInfo;
        Location source, destination;
        Person carrier;
        
        carrierRequestInfo = new CarrierRequestInfo();
        
        requestInfo = new CarrierRequestInfo.RequestInfo();
        requestInfo.setRequestId(option.getOptionId());
        
        source = new Location();
        source.setLatitude(option.getOffer().getSourceLatitude());
        source.setLongitude(option.getOffer().getSourceLongitude());
        source.setName(option.getOffer().getSourceName());
        
        destination = new Location();
        destination.setLatitude(option.getOffer().getDestinationLatitude());
        destination.setLongitude(option.getOffer().getDestinationLongitude());
        destination.setName(option.getOffer().getDestinationName());
        
        requestInfo.setSource(source);
        requestInfo.setDestination(destination);
        
        requestInfo.setPickupTime(option.getOffer().getDateOfPickup());
        requestInfo.setDeliveryTime(option.getOffer().getDateOfDelivery());
        requestInfo.setMaximumParcelWeight(option.getOffer().getWeight());
        
        carrier = PersonParser.entityToServiceModel(option.getOffer().getUserInfo());
        
        carrierRequestInfo.setRequestInfo(requestInfo);
        carrierRequestInfo.setCarrier(carrier);
        
        return carrierRequestInfo;
    }
    
    public static CreateSearchResponse.CarrierOffers optionsToCarrierOffers(List<Option> options) {
        CreateSearchResponse.CarrierOffers carrierOffers = new CreateSearchResponse.CarrierOffers();
        CarrierRequestInfo requestInfo;
        
        for (Option option: options) {
            requestInfo = optionToCarrierRequestInfo(option);
            carrierOffers.getCarrierOffer().add(requestInfo);
        }
        
        return carrierOffers;
    }
    
    public static SenderRequestInfo optionToSenderRequestInfo(Option option) {
        SenderRequestInfo senderRequestInfo;
        SenderRequestInfo.RequestInfo requestInfo;
        Location source, destination;
        Person sender;
        
        senderRequestInfo = new SenderRequestInfo();
        
        requestInfo = new SenderRequestInfo.RequestInfo();
        requestInfo.setRequestId(option.getOptionId());
        
        source = new Location();
        source.setLatitude(option.getSearch().getSourceLatitude());
        source.setLongitude(option.getSearch().getSourceLongitude());
        source.setName(option.getSearch().getSourceName());
        
        destination = new Location();
        destination.setLatitude(option.getSearch().getDestinationLatitude());
        destination.setLongitude(option.getSearch().getDestinationLongitude());
        destination.setName(option.getSearch().getDestinationName());
        
        requestInfo.setSource(source);
        requestInfo.setDestination(destination);
        
        requestInfo.setPickupTime(option.getSearch().getDateOfPickup());
        requestInfo.setDeliveryTime(option.getSearch().getDateOfDelivery());
        
        requestInfo.setParcelDescription(option.getSearch().getParcelDescription());
//        requestInfo.setParcelType(option.getSearch().getParcelCategory());
        requestInfo.setParcelWeight(option.getSearch().getParcelWeight());
        
        
        sender = PersonParser.entityToServiceModel(option.getSearch().getUserInfo());
        
        senderRequestInfo.setRequestInfo(requestInfo);
        senderRequestInfo.setSender(sender);
        
        return senderRequestInfo;
    }
    
    public static CreateOfferResponse.SenderRequests optionsToSenderRequests(List<Option> options) {
        CreateOfferResponse.SenderRequests senderRequests = new CreateOfferResponse.SenderRequests();
        SenderRequestInfo requestInfo;
        
        for (Option option: options) {
            requestInfo = optionToSenderRequestInfo(option);
            senderRequests.getSenderRequest().add(requestInfo);
        }
        
        return senderRequests;
    }
}
