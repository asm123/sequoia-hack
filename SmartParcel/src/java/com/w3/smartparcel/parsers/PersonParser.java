/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.w3.smartparcel.parsers;

import com.w3.smartparcel.entities.UserInfo;
import com.w3.smartparcel.servicemodels.Person;

/**
 *
 * @author asmita
 */
public class PersonParser {
    
    public static Person entityToServiceModel(UserInfo user) {
        Person person = new Person();
        
        person.setName(user.getName());
        person.setEmailID(user.getEmail());
        person.setPhoneNumber(user.getPhoneNumber());
        if (user.getPicture() != null) {
            person.setPicture(new String(user.getPicture()));
        }
        
        return person;
    }
    
}
