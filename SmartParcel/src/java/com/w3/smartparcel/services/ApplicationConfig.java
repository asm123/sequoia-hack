/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.w3.smartparcel.services;

import java.util.Set;
import javax.ws.rs.core.Application;

/**
 *
 * @author asmita
 */
@javax.ws.rs.ApplicationPath("")
public class ApplicationConfig extends Application {

    @Override
    public Set<Class<?>> getClasses() {
        Set<Class<?>> resources = new java.util.HashSet<>();
        addRestResourceClasses(resources);
        return resources;
    }

    /**
     * Do not modify addRestResourceClasses() method.
     * It is automatically populated with
     * all resources defined in the project.
     * If required, comment out calling this method in getClasses().
     */
    private void addRestResourceClasses(Set<Class<?>> resources) {
        resources.add(com.w3.smartparcel.services.BookingFacadeREST.class);
        resources.add(com.w3.smartparcel.services.LoginFacadeREST.class);
        resources.add(com.w3.smartparcel.services.OfferFacadeREST.class);
        resources.add(com.w3.smartparcel.services.OptionFacadeREST.class);
        resources.add(com.w3.smartparcel.services.SearchFacadeREST.class);
        resources.add(com.w3.smartparcel.services.TrackingFacadeREST.class);
        resources.add(com.w3.smartparcel.services.UserFacadeREST.class);
    }
    
}
