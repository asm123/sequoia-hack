/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.w3.smartparcel.services;

import com.w3.smartparcel.applicationlayer.OptionHandler;
import com.w3.smartparcel.applicationlayer.UserHandler;
import com.w3.smartparcel.entities.Option;
import com.w3.smartparcel.entities.UserInfo;
import com.w3.smartparcel.servicemodels.ConfirmRequestFromSender;
import com.w3.smartparcel.servicemodels.CreateRequestToCarrier;
import com.w3.smartparcel.servicemodels.CreateRequestToSender;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author asmita
 */
@Stateless
@Path("Option")
public class OptionFacadeREST extends AbstractFacade<Option> {

    @PersistenceContext(unitName = "SmartParcelPU")
    private EntityManager em;

    public OptionFacadeREST() {
        super(Option.class);
    }

    @PUT
    @Consumes({MediaType.APPLICATION_XML})
    @Produces({MediaType.APPLICATION_XML})
    @Path("/SenderRequest")
    public Response sendRequestToCarrier(@HeaderParam("userId") int userId, CreateRequestToCarrier requestToCarrier) {
        UserInfo sender;
        boolean status;
        
        try {
            sender = UserHandler.getUser(em, userId);
            status = new OptionHandler(em).sendRequestToCarrier(requestToCarrier, sender);
        } catch (Exception e) {
            e.printStackTrace();
            return Response.status(Response.Status.UNAUTHORIZED).build();
        }
        return Response.status(Response.Status.CREATED).build();
    }
    
    @PUT
    @Consumes({MediaType.APPLICATION_XML})
    @Produces({MediaType.APPLICATION_XML})
    @Path("/CarrierRequest")
    public Response sendRequestToSender(@HeaderParam("userId") int userId, CreateRequestToSender requestToSender) {
        UserInfo carrier;
        boolean status;
        
        try {
            carrier = UserHandler.getUser(em, userId);
            status = new OptionHandler(em).sendRequestToSender(requestToSender, carrier);
        } catch (Exception e) {
            e.printStackTrace();
            return Response.status(Response.Status.UNAUTHORIZED).build();
        }
        return Response.status(Response.Status.CREATED).build();
    }
    
    @PUT
    @Consumes({MediaType.APPLICATION_XML})
    @Produces({MediaType.APPLICATION_XML})
    @Path("/SenderRequest/{id}")
    public Response confirmRequestFromSender(@HeaderParam("userId") int userId, @QueryParam("requestId") int requestId,
            ConfirmRequestFromSender requestFromSender) {
        UserInfo carrier;
        boolean status;
        
        try {
            carrier = UserHandler.getUser(em, userId);
            status = new OptionHandler(em).confirmRequestFromSender(requestFromSender, carrier, requestId);
        } catch (Exception e) {
            e.printStackTrace();
            return Response.status(Response.Status.UNAUTHORIZED).build();
        }
        return Response.status(Response.Status.CREATED).build();
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    
}
