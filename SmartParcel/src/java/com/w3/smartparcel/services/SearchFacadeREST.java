/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.w3.smartparcel.services;

import com.w3.smartparcel.applicationlayer.SearchHandler;
import com.w3.smartparcel.applicationlayer.UserHandler;
import com.w3.smartparcel.entities.Search;
import com.w3.smartparcel.entities.UserInfo;
import com.w3.smartparcel.parsers.SearchParser;
import com.w3.smartparcel.servicemodels.CreateSearchRequest;
import com.w3.smartparcel.servicemodels.CreateSearchResponse;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author asmita
 */
@Stateless
@Path("Search")
public class SearchFacadeREST extends AbstractFacade<Search> {

    @PersistenceContext(unitName = "SmartParcelPU")
    private EntityManager em;

    public SearchFacadeREST() {
        super(Search.class);
    }
    
    @POST
    @Consumes({MediaType.APPLICATION_XML})
    @Produces({MediaType.APPLICATION_XML})
    public Response searchOffers(@HeaderParam("userId") int userId, CreateSearchRequest searchRequest) {
        SearchHandler handler;
        CreateSearchResponse response;
        UserInfo sender;
        Search search;
        
        try {
            handler = new SearchHandler(em);
            sender = UserHandler.getUser(em, userId);
            search = SearchParser.serviceModelToEntity(searchRequest, sender);
            response = handler.searchOffers(search);
        } catch (Exception e) {
            e.printStackTrace();
            return Response.status(Response.Status.UNAUTHORIZED).build();
        }
        
        return Response.status(Response.Status.CREATED).entity(response).build();
    }

    /*
    @POST
    @Override
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public void create(Search entity) {
        super.create(entity);
    }

    @PUT
    @Path("{id}")
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public void edit(@PathParam("id") Integer id, Search entity) {
        super.edit(entity);
    }

    @DELETE
    @Path("{id}")
    public void remove(@PathParam("id") Integer id) {
        super.remove(super.find(id));
    }

    @GET
    @Path("{id}")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public Search find(@PathParam("id") Integer id) {
        return super.find(id);
    }

    @GET
    @Override
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public List<Search> findAll() {
        return super.findAll();
    }

    @GET
    @Path("{from}/{to}")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public List<Search> findRange(@PathParam("from") Integer from, @PathParam("to") Integer to) {
        return super.findRange(new int[]{from, to});
    }

    @GET
    @Path("count")
    @Produces(MediaType.TEXT_PLAIN)
    public String countREST() {
        return String.valueOf(super.count());
    }
    */
    
    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    
}
